<?php

error_reporting(E_ERROR | E_PARSE);
ini_set("max_execution_time", 60);

// Database/IRC/irker config
require('config.php');

// Check if point lies within polygon
require('polygonCheck.php');
// UT polygon information
require('utwentePolygon.php');
// Calculate lat-long detection
list($constant, $multiple) = precalcValues($polyLat, $polyLong);

// Define a curl file getter for better error handling
function file_get_contents_curl($url) {
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

  $data = curl_exec($ch);
  curl_close($ch);

  if ($data === FALSE) {
    throw new Exception();
  }

  return $data;
}

// Create database connection
$db = mysqli_connect($dbHost, $dbUser, $dbPass, $dbUser);
if (mysqli_connect_errno()) {
  printf("Connect failed: %s\n%s", mysqli_connect_errno(), mysqli_connect_error());
  exit();
}

// Get the feed data of Brandweer
try {
  $feedBrandweer = file_get_contents_curl('http://feeds.livep2000.nl/?r=5&d=1');
  $xmlBrandweer  = simplexml_load_string($feedBrandweer);
} catch (Exception $e) {
  $xmlBrandweer = NULL;
}

// Get the feed data of Ambulance
try {
  $feedAmbulance = file_get_contents_curl('http://feeds.livep2000.nl/?r=5&d=2');
  $xmlAmbulance  = simplexml_load_string($feedAmbulance);
} catch (Exception $e) {
  $xmlAmbulance = NULL;
}

// Get the feed data of Politie
try {
  $feedPolitie = file_get_contents_curl('http://feeds.livep2000.nl/?r=5&d=3');
  $xmlPolitie  = simplexml_load_string($feedPolitie);
} catch (Exception $e) {
  $xmlPolitie = NULL;
}

// Combine xml data
$xmls = array(
    $xmlBrandweer,
    $xmlAmbulance,
    $xmlPolitie,
);

// Add mIRC-colours: Brandweer in red, Politie in blue, Ambulance in green.
$codes = array(
    chr(03) . '04Brandweer' . chr(03),
    chr(03) . '09Ambulance' . chr(03),
    chr(03) . '12Politie' . chr(03),
);

// Define UT streets
$streets = array(
    'universiteit', 'calslaan', 'campuslaan',
    'de hems', 'matenweg', 'witbreuksweg',
    'achterhorst', 'drienerbeeklaan', 'bosweg',
    'boerderijweg', 'de horst', 'de knepse',
    'de zul', 'drienerlolaan', 'hallenweg',
    'horstlindelaan', 'het ritke',
    'langenkampweg', 'oude drienerlo weg', 'oude horstlindeweg',
    'parallelweg noord', 'parallelweg zuid', 'reelaan',
    'van heeksbleeklaan', 'viermarkenweg', 'zomerdijksweg',
);

// Ignore false positive streets
$ignoredStreets = array(
    'krabbenbosweg', 'grintenbosweg', 'bosmatenweg',
    'schabosweg', 'duivelshofbosweg',
);

/** @var SimpleXMLElement $entry */
for ($i = 0; $i < count($xmls); $i++) {

  $xml = $xmls[$i];

  if ($xml === NULL) continue;

  foreach ($xml->channel->item as $entry) {
    // Check entry guid
    if (!is_numeric((string)$entry->guid)){
      continue;
    }

    // Check if guid already posted
    $query  = 'SELECT guid FROM posted WHERE guid = ' . mysqli_escape_string($db, $entry->guid);
    $result = mysqli_query($db, $query);
    if (mysqli_errno($db)) {
      printf("Select failed: \"%s\"\n", $query);
      printf("Select failed: %s\n%s", mysqli_errno($db), mysqli_error());
      exit();
    }
    if (mysqli_num_rows($result) != 0) {
      continue;
    }

    // Get geo information
    $namespaces = $entry->getNameSpaces(true);
    if (array_key_exists('geo', $namespaces)) {
      $geo  = $entry->children($namespaces['geo']);
      $lat  = $geo->lat;
      $long = $geo->long;
    } else {
      $lat  = 0;
      $long = 0;
    }

    $match      = false;
    $matchOnGeo = false;
    if (isset($geo) && $lat && $long) {
      // Check for match on geo
      $match      = pointInPolygon($polyLat, $polyLong, $constant, $multiple, (float)$lat, (float)$long);
      $matchOnGeo = $match;
    } else {
      // Check for match on street
      foreach ($streets as $street) {
        if (stripos($entry->title, $street) !== false) {
          // Also check whether the street isn't ignored
          foreach ($ignoredStreets as $ignoredStreet) {
            if (stripos($entry->title, $ignoredStreet) !== false) {
              continue 2;
            }
          }

          $match = true;
          break;
        }
      }
    }

    // Determine if posted or not
    $sqlDateTime = 'NULL';
    if ($match) {
      $sqlDateTime = 'NOW()';
    }

    // Create DateTime from pubData
    $datetime = DateTime::createFromFormat('D, d M Y H:i:s O', $entry->pubDate);

    // Put P2000 into database
    $query  = sprintf('INSERT INTO posted(guid, posted, geomatched, notif_type, title, description, pubDate, pubDateOrig, geolat, geolong) VALUES ("%s", %s, %d, %d, "%s", "%s", "%s", "%s", "%s", "%s")',
        mysqli_escape_string($db, $entry->guid),
        $sqlDateTime,
        $matchOnGeo,
        $i,
        mysqli_escape_string($db, $entry->title),
        mysqli_escape_string($db, $entry->description),
        $datetime->format('Y-m-d H:i:s'),
        mysqli_escape_string($db, $entry->pubDate),
        mysqli_escape_string($db, $lat),
        mysqli_escape_string($db, $long)
    );
    $result = mysqli_query($db, $query);
    if (mysqli_errno($db)) {
      printf("Insert failed: %s\n%s", mysqli_errno($db), mysqli_error($db));
      exit();
    }

    // Not a matched, skip IRC
    if (!$match) {
      continue;
    }

    // Remove trailing ~ from message, if any
    $title = $entry->title;
    if (substr($title, -1) == '~') {
      $title = substr($title, 0, -1);
    }

    // Put it on IRC
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    socket_connect($socket, $irkerServer, $irkerPort);
    $data = json_encode(array(
        'to'      => $ircTo,
        "privmsg" => $datetime->format('H:i:s') . ' ' . $codes[$i] . ' ' . $title,
    ));
    socket_send($socket, $data, strlen($data), MSG_EOF);
    socket_close($socket);
  }
}
