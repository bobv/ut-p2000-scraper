This is a simple script which parses a XML file with p2000 from http://feeds.livep2000.nl/?r=5 and puts it, using irking, on IRC. 

Usage: `php -f p2000scraper.php`

Do not forget to set the config accordingly to your needs.
 
The current script uses an UT polygon as defined in [issue #2](https://git.snt.utwente.nl/bobv/utwente-p2000-scraper/issues/2 "Issue #2") by Frans van Dijk. See the include utwente.pdf for a map.